package com.example.projecttahel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class BasicActivity extends AppCompatActivity {
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main_menu, menu);
        if(BackgroundMusicService.isPlaying) {
            playMusic();
        }
        else{
            stopMusic();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.muteMusic:
                playMusic();
                break;

            case R.id.highMusic:
                stopMusic();
                break;

            default:
                break;
        }
        return true;
    }

    private void playMusic(){
        BackgroundMusicService.startMusic();
        menu.getItem(1).setVisible(false);
        menu.getItem(0).setVisible(true);
    }

    private void stopMusic(){
        BackgroundMusicService.stopMusic();
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
    }
}