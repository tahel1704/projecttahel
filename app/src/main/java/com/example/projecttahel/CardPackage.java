package com.example.projecttahel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

/**
 * this class is meant to represent a package of cards in the game
 * each package contain:
 *      the deck,
 *      the used cards,
 *      the last card that one of the users thrown
 * this class is responsible to all the things we need to do with the cards in the game
 * (taking a new random card from the deck, throw or taking a used card, connecting card's value and img...)
 */

public class CardPackage {
    private ArrayList<Integer> deckCards; //the deck of the cards
    private ArrayList<Integer> usedCards; //the cards that the players already used
    private Card lastThrownCard; //the last card that any player thrown to the used-cards
    private HashMap<String, Integer> cardsImages; //matches the value of a card to its image

    public CardPackage() {
        this.deckCards = new ArrayList<Integer>(Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 9, 3, 3, 3));
        usedCards = new ArrayList<Integer>(Collections.nCopies(deckCards.size(), 0));

        //creating the map of value-img to each kind of card
        cardsImages = new HashMap<String, Integer>();
        cardsImages.put("0" + "_i", R.drawable.zero);
        cardsImages.put("1" + "_i", R.drawable.one);
        cardsImages.put("2" + "_i", R.drawable.two);
        cardsImages.put("3" + "_i", R.drawable.three);
        cardsImages.put("4" + "_i", R.drawable.four);
        cardsImages.put("5" + "_i", R.drawable.five);
        cardsImages.put("6" + "_i", R.drawable.six);
        cardsImages.put("7" + "_i", R.drawable.seven);
        cardsImages.put("8" + "_i", R.drawable.eight);
        cardsImages.put("9" + "_i", R.drawable.nine);
        cardsImages.put("10" + "_i", R.drawable.peek); //peek card
        cardsImages.put("11" + "_i", R.drawable.pulltwo); //pull-two card
        cardsImages.put("12" + "_i", R.drawable.swap); //swap card

        lastThrownCard = this.getRandomCard(true); //init the first open card (the first used card)
    }

    /**
     * checks if the package is empty - the amount of all the cards is 0
     * @return true if empty, false if not
    */
    private boolean isDeckEmpty(){
        //checking if contain specials card
        for(int i = this.deckCards.size()-3; i < this.deckCards.size(); i++) {
            if (this.deckCards.get(i) != 0){
                return false;
            }
        }
        //if not contains special cards, checking if contain regular cards
        return this.isDeckNotContainsRegulars();
    }

    /**
     * checks if the deck not contain regular cards
     * @return false if contain a regular card, true if not
     */
    private boolean isDeckNotContainsRegulars(){
        for(int i = 0; i < this.deckCards.size()-3; i++) {
            if (this.deckCards.get(i) != 0){
                return false;
            }
        }
        return true;
    }

    /**
     * swapping the deck-cards and the garbage-cards.
     */
    private void swapPackages(){
        for(int i = 0 ; i<this.deckCards.size();i++){
            this.deckCards.set(i, this.deckCards.get(i) - this.usedCards.get(i));
            this.usedCards.set(i, this.deckCards.get(i) + this.usedCards.get(i));
            this.deckCards.set(i, Math.abs(this.deckCards.get(i) - this.usedCards.get(i)));
        }
    }

    /**
     * the function generate a random number of a card (0-12),
     * checks if the cards of this value hadn't over yet
     * and create a new Card with the value / generate a new random number
     * (until it gets to a value that hadn't over yet)
     * @return a new Card with the random value
     * @param onlyRegulars: if we want to have only regulars cards
     */
    public Card getRandomCard(boolean onlyRegulars){
        Random rand = new Random();
        int value = -1;
        if (this.isDeckNotContainsRegulars() || this.isDeckEmpty()){
            this.swapPackages();
        }
        do {
            if (onlyRegulars) {
                value = rand.nextInt(10); //0-9
            }
            else {
                value = rand.nextInt(13); //0-12
            }
        } while (this.deckCards.get(value) <= 0); //checks if the value is valid
        this.deckCards.set(value, this.deckCards.get(value) - 1); //remove 1 from the amount of that card
        return new Card(value, cardsImages.get(value+ "_i"));
    }
    

    /**
     * adding 1 to the amount of the cards.
     * use when the user throw a card
     * @param card: the card to throw
     */
    public void throwCard(Card card){
        this.usedCards.set(card.getValue(), this.usedCards.get(card.getValue()) + 1);
        this.lastThrownCard = card;
    }

    /**
     * removing 1 from the amount of the card.
     * used when the user take the upper card
     * @return the last thrown card
     */
    public Card takeLastUsedCard(){
        this.usedCards.set(this.lastThrownCard.getValue(), this.usedCards.get(this.lastThrownCard.getValue()) - 1);
        return this.lastThrownCard;
    }


    /***********          GETTERS & SETTERS          **********/

    public Card getLastUsedCard(){
        return this.lastThrownCard;
    }

    public ArrayList<Integer> getDeckCards() {
        return deckCards;
    }

    public void setDeckCards(ArrayList<Integer> deckCards) {
        this.deckCards = deckCards;
    }

    public ArrayList<Integer> getUsedCards() {
        return usedCards;
    }

    public void setUsedCards(ArrayList<Integer> usedCards) {
        this.usedCards = usedCards;
    }

    public Card getLastThrownCard() {
        return lastThrownCard;
    }

    public void setLastThrownCard(Card lastThrownCard) {
        this.lastThrownCard = lastThrownCard;
    }

    public HashMap<String, Integer> getCardsImages() {
        return cardsImages;
    }

    public void setCardsImages(HashMap<String, Integer> cardsImages) {
        this.cardsImages = cardsImages;
    }

}
