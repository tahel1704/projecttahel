package com.example.projecttahel;

import android.graphics.Bitmap;

/**
 * the class saves the user's data and statistics:
 *      username, total score, number of games...
 */
public class UserStatistics
{
    private String userName;
    private int  score;
    private int games;

    public UserStatistics(){}

    public UserStatistics(String userName) {
        this.userName = userName;
        this.score = 0;
        this.games = 0;
    }

    public void addScore(int scoreToAdd){
        this.score += scoreToAdd;
    }

    public void addGame(){
        this.games ++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserStatistics userStatistics = (UserStatistics) o;
        return score == userStatistics.score &&
                games == userStatistics.games &&
                userName.equals(userStatistics.userName);
    }


    /***********          GETTERS & SETTERS          **********/

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }
}