package com.example.projecttahel;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class BackgroundMusicService extends Service {
    static MediaPlayer mediaPlayer; //the music
    static boolean isPlaying; //showing is the user hears the music or not

    public BackgroundMusicService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mediaPlayer.start();
        return 1;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = MediaPlayer.create(this, R.raw.smurfmusic);
        mediaPlayer.setLooping(true);
        mediaPlayer.setVolume(1f,1f);
        isPlaying = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.release();
        isPlaying = false;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void onStop() {
        mediaPlayer.stop();
    }

    static public void startMusic() {
        mediaPlayer.setVolume(1.0f, 1.0f);
        isPlaying = true;
    }

    static public void stopMusic() {
        mediaPlayer.setVolume(0f,0f);
        isPlaying = false;
    }

}