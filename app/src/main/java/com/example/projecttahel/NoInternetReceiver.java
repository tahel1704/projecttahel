package com.example.projecttahel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class NoInternetReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (!(wifi != null && wifi.isConnectedOrConnecting()) && !(mobile != null && mobile.isConnectedOrConnecting()) ) {
            Toast.makeText(context, "NO INTERNET CONNECTION", Toast.LENGTH_LONG).show();
        }
    }
}