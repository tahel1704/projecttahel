package com.example.projecttahel;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class WaitingRoom extends BasicActivity implements View.OnClickListener {
    //xml things
    ListView usersListView;
    Button btnStartGame;
    ImageButton btnBack;

    ArrayList<Player> userList =new ArrayList<>(); //list of the users in the game
    public static GameRoom gameRoom; //the game-room that we want to enter to
    UserStatistics user; //the user that playes
    Boolean foundRoom; //if we found the room that the user wants to connect to

    //db things
    FirebaseDatabase database;
    ValueEventListener listener;
    Query q;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);

        //init the buttons
        btnStartGame = findViewById(R.id.btn_start_game);
        btnStartGame.setOnClickListener(this);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        //init the db things
        database = FirebaseDatabase.getInstance();

        //init the variables
        usersListView = (ListView) findViewById(R.id.usersList);
        gameRoom = JoinCreateRoom.gameRoom;
        user = MainActivity.user.stat;
        foundRoom = false;

        //checking if this user is the admin - if he is, he can start the game
        checkIfAdmin();

        //adding a listener in case a user join or log out and update the game-room
        q = database.getReference().child("game-rooms").orderByKey();
        listener = q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dst : snapshot.getChildren()) { //move on all the game-rooms and checks if its the one the user is in
                    GameRoom room = dst.getValue(GameRoom.class);
                    if (room.getGameRoomId() == gameRoom.getGameRoomId()) {
                        gameRoom = room;
                        foundRoom = true;
                        updateUsersInList();
                    }
                }

                if (!foundRoom)
                {
                    Intent i = new Intent(WaitingRoom.this, JoinCreateRoom.class);
                    startActivity(i);
                }

                //checking if the user is the admin - if he is, he can start the game
                checkIfAdmin();
                if (foundRoom && gameRoom.isGameStart()){
                    Intent in = new Intent(WaitingRoom.this, Game.class);
                    startActivity(in);
                }
                foundRoom = false;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                removeUserFromRoom();
                break;

            case R.id.btn_start_game:
                if (gameRoom.getUsers().size() == 2) {
                    gameRoom.startGame();
                    database.getReference().child("game-rooms").child(String.valueOf(gameRoom.getGameRoomId())).setValue(gameRoom);
                    Intent in = new Intent(WaitingRoom.this, Game.class);
                    startActivity(in);
                }
                else{
                    Toast.makeText(WaitingRoom.this, "NEED 2 PLAYERS TO PLAY", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        q.removeEventListener(listener);
    }


    /*
    * when users go in / out the room' we want the other users in the room to see it
    * the function update the users in the list-view
    */
    private void updateUsersInList(){
        userList.clear(); //clear the users-list (now there is no users in it), we don't want duplication
        userList.addAll(gameRoom.getUsers()); //adding all the users again
        //updating the list view (on the screen)
        MyAdapter myAdapter=new MyAdapter(this,R.layout.activity_waiting_room,userList);
        usersListView.setAdapter(null);
        usersListView.setAdapter(myAdapter);
    }

    /*
     *checking if the user is the admin (the first in the list) and hide / show the start button
     * if he is, he have admin permission like starting the game
     */
    private void checkIfAdmin(){
        if (!gameRoom.isPlayerAdmin(MainActivity.user.stat)) { //if the user is not the admin
            btnStartGame.setVisibility(View.GONE);
        }
        else{ //if the user is the admin
            btnStartGame.setVisibility(View.VISIBLE);
        }
    }

    /*
     * remove the user from the room in the db
     * if the user is the last one in the room, it delete the whole room in the db
     */
    private void removeUserFromRoom(){
        if(gameRoom.isPlayerAdmin(user)){
            database.getReference().child("game-rooms").child(String.valueOf(gameRoom.getGameRoomId())).removeValue();
            Intent i = new Intent(WaitingRoom.this, JoinCreateRoom.class);
            startActivity(i);
        }

        else{
            gameRoom.removeUser(user); //remove the user from the game-room (from the users list)
            database.getReference().child("game-rooms").child(String.valueOf(gameRoom.getGameRoomId())).setValue(gameRoom);
            Intent i = new Intent(WaitingRoom.this, JoinCreateRoom.class);
            startActivity(i);
        }
    }
}