package com.example.projecttahel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class JoinCreateRoom extends BasicActivity implements View.OnClickListener {
    //db things
    FirebaseDatabase database;
    DatabaseReference myRef;

    public static GameRoom gameRoom; //the game-room that the player joined/created
    String gameId; //the game-id that the user entered

    //xml things
    EditText etId;
    Button btnCreate, btnJoin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_create_room);

        gameRoom = null;

        //init the buttons and the edit-text
        btnCreate = findViewById(R.id.btn_create);
        btnCreate.setOnClickListener(this);
        btnJoin = findViewById(R.id.btn_join);
        btnJoin.setOnClickListener(this);
        etId = findViewById(R.id.etId);

        //init the db things
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        gameId = etId.getText().toString();
        if (!gameId.equals("")) { //if the user entered a game-id
            switch (v.getId()) {
                //if the user want to create a new room
                case R.id.btn_create:
                    createGameRoom(gameId);
                    break;

                //if the user want to join an exist room
                case R.id.btn_join:
                    joinGameRoom(gameId);
                    break;
            }
        }
    }


    /**
     * checks if the room-id already exist.
     * if not- it creates a new room and push it to the db and then goes to the waiting room
     */
    private void createGameRoom(String id) {
        Query q = database.getReference().child("game-rooms").orderByValue();
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                //checking if the room-id already exist
                boolean isExist = false;
                for (DataSnapshot dst : snapshot.getChildren()) { //move on all the game-id and checks if its the same as the new one
                    if (id.equals(dst.getKey())) {
                        isExist = true;
                    }
                }

                //if the game-id already exist - make a toast
                if (isExist){
                    Toast.makeText(JoinCreateRoom.this, "game id already exist", Toast.LENGTH_LONG).show();
                }

                //if the game-id does't exist
                else{
                    //creates a new game room with this user in it and push to the db
                    gameRoom = new GameRoom(MainActivity.user, Integer.parseInt(id));
                    myRef = database.getReference().child("game-rooms");
                    myRef.child(id).setValue(gameRoom);

                    //go to the waiting room
                    Intent i = new Intent(JoinCreateRoom.this, WaitingRoom.class);
                    startActivity(i);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    /**
     * checks if the room-id already exist.
     * if it is- it joins the room and update it in the db and then goes to the waiting room
     */
    private void joinGameRoom(String id){
        //checks in the db if the room-id already exist
        Query q = database.getReference().child("game-rooms").orderByValue();
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean isExist = false;

                //move on all the game-id and checks if its the same as the new one
                for (DataSnapshot dst : snapshot.getChildren()) {
                    gameRoom = dst.getValue(GameRoom.class);

                    //if exist
                    if (id.equals(dst.getKey())) {
                        isExist = true;

                        // if the room is full
                        if (gameRoom.getUsers().size() >= 2){
                            Toast.makeText(JoinCreateRoom.this, "No place in this room", Toast.LENGTH_LONG).show();
                        }
                        //if there is a place for another player in the room
                        else {
                            //update the game-room and the db with the new user
                            gameRoom.addUser(MainActivity.user);
                            database.getReference().child("game-rooms").child(id).setValue(gameRoom);

                            //go to the waiting room
                            Intent i = new Intent(JoinCreateRoom.this, WaitingRoom.class);
                            startActivity(i);
                        }
                    }
                }

                //if the game-id does't exist - make a toast
                if (!isExist){
                    Toast.makeText(JoinCreateRoom.this, "GAME ID doesn't exist", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameRoom = null;
    }

}