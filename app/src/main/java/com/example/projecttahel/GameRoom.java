package com.example.projecttahel;

import java.util.ArrayList;

/**
 * this class is response to the game moves.
 * the class contain:
 *      the game-room id,
 *      if the game already started or finished
 *      all the users in the game (max of 2)
 *      the cards package
 * the class know how to manage the users (adding and removing),
 * starting and finishing the game,
 * swapping cards between 2 players
 */

public class GameRoom {
    private int gameRoomId;             //the room id
    private ArrayList<Player> users;    //the users list (2 users max)
    private boolean isGameStart;        //if the game started (false if the players in the waiting room, true if in the game)
    private int currPlayerIndex;        //the current player index
    private CardPackage pack;           //the pack of cards
    private boolean finishGame;         //if the game have been finished - one of the players wanted to finish it
    private ArrayList<Integer> swappedCardIndexes;

    public GameRoom(Account admin, int id) {
        this.gameRoomId = id;
        this.currPlayerIndex = 0;
        this.users = new ArrayList<Player>();
        this.pack = new CardPackage();
        this.users.add(new Player(admin.stat, pack));
        this.isGameStart = false;
        this.finishGame = false;

        this.swappedCardIndexes = new ArrayList<Integer>();
        this.swappedCardIndexes.add(-1);
        this.swappedCardIndexes.add(-1);
    }

    public GameRoom(int gameRoomId, ArrayList<Player> users) {
        this.gameRoomId = gameRoomId;
        this.users = users;
        this.pack = new CardPackage();
        this.currPlayerIndex = 0;
    }

    public GameRoom() {}

    /**
     * adding a new user to the game-room's users list
     * @param user: the user to add to the users
     */
    public void addUser(Account user){
        this.users.add(new Player(user.stat, pack));
    }

    /**
     * removes the user from the game-room's users list
     * @param user: the user to remove from the game
     */
    public void removeUser(UserStatistics user) {
        for (Player player : this.users)
        {
            if (player.getUser().equals(user))
            {
                this.users.remove(player);
            }
        }
    }

    /**
     * returns the game-room's admin (the first user in the list)
     * @return: the player of the admin
     */
    public Player getAdmin() { return this.users.get(0);}

    public void startGame() {this.isGameStart = true;}

    public void finishGame() {this.finishGame = true;}

    /**
     * adding the score to the user & adding one game
     * @param playerIndex - the player's index
     */
    public void updatePlayerGameOver(int playerIndex){
        this.users.get(playerIndex).getUser().addGame();
        this.users.get(playerIndex).getUser().addScore(this.users.get(playerIndex).clcSum(this.pack));
    }

    /**
     * the class saves the swapped cards that the user chose after getting swap-card
     * @param player0card: the card of player in index 0
     * @param player1card: the card of player in index 1
     */
    public void swappedCards(int player0card, int player1card){
        this.swappedCardIndexes.set(0, player0card);
        this.swappedCardIndexes.set(1, player1card);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameRoom room = (GameRoom) o;
        return gameRoomId == room.gameRoomId &&
                isGameStart == room.isGameStart &&
                currPlayerIndex == room.currPlayerIndex &&
                finishGame == room.finishGame &&
                users.equals(room.users) &&
                pack.equals(room.pack);
    }

    /**
     * clc the player's points
     */
    public void clcPoints(){
        for (Player player : this.users){
            player.setGameScore(player.clcSum(this.pack));
        }
    }

    /**
     * getting the player by his index
     * @param playerIndex: the index of the player
     * @return: the player
     */
    public Player getPlayerByIndex(int playerIndex){
        return this.users.get(playerIndex);
    }

    /**
     * checks if the player is the admin of the game
     * @return: true- if he is, false- if not
     */
    public boolean isPlayerAdmin(int playerIndex){
        return this.getAdmin().getUser().equals(this.getPlayerByIndex(playerIndex).getUser());
    }
    public boolean isPlayerAdmin(UserStatistics user){
        return this.getAdmin().getUser().equals(user);
    }


    /**********     GETTER & SETTERS     **********/

    public int getGameRoomId() {
        return gameRoomId;
    }

    public void setGameRoomId(int gameRoomId) {
        this.gameRoomId = gameRoomId;
    }

    public ArrayList<Player> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<Player> users) {
        this.users = users;
    }

    public boolean isGameStart() {
        return isGameStart;
    }

    public void setGameStart(boolean gameStart) {
        isGameStart = gameStart;
    }

    public int getCurrPlayerIndex() {
        return currPlayerIndex;
    }

    public void setCurrPlayerIndex(int currPlayerIndex) {
        this.currPlayerIndex = currPlayerIndex;
    }

    public CardPackage getPack() {
        return pack;
    }

    public void setPack(CardPackage pack) {
        this.pack = pack;
    }

    public boolean getFinishGame() {
        return finishGame;
    }

    public void setFinishGame(boolean finishGame) {
        this.finishGame = finishGame;
    }

    public ArrayList<Integer> getSwappedCardIndexes() {
        return swappedCardIndexes;
    }

    public void setSwappedCardIndexes(ArrayList<Integer> swappedCardIndexes) {
        this.swappedCardIndexes = swappedCardIndexes;
    }

}
