package com.example.projecttahel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Game extends BasicActivity implements View.OnClickListener {
    //xml things
    ImageButton deckBtn, usedCardsBtn;
    ImageButton card1Btn, card2Btn, card3Btn, card4Btn;
    ImageButton userCard1Btn, userCard2Btn, userCard3Btn, userCard4Btn;
    Button finishGameBtn, gameOverBtn;
    TextView player2nameTV, userNameTV;
    ImageView finishGameIv;

    //db things
    FirebaseDatabase database;
    DatabaseReference myRef;
    Query q;
    ValueEventListener listener;

    GameRoom gameRoom; //the game-room that the player plays in
    public static int playerIndex; //the player's index (public static because we want to use in in the after-game-intent)
    int numOfPlayers; //the number of players (suppose to be 2)
    boolean peekCard, pullTwoCard, swapCard; //if the user got a special card
    boolean switchOldCardWithNew; //if the player took a card and now needs to place it in one of his cards
    boolean finished; //if the game finished
    int chosenCardIndex, chosenOpponentCardIndex; //in case of swapping cards - saves the indexes of the cards to swap
    Card currCard; //the players card for this turn. can be a whole new card from the deck or a used one
    public static int player_score, opponent_score; //the players score after the game (public static because we want to use in in the after-game-intent)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //init the variables
        switchOldCardWithNew = false;
        peekCard = false;
        pullTwoCard = false;
        swapCard = false;
        finished = false;
        chosenCardIndex = -1;
        chosenOpponentCardIndex = -1;

        //init the db things
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("game-rooms");

        //gets the game-room
        gameRoom = WaitingRoom.gameRoom;

        numOfPlayers = gameRoom.getUsers().size();
        find_player_index();

        //listening to any changes in the firebase about the game-room
        q = myRef.orderByValue();
        listener = q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean found = false; //the game found?
                //move on all the game-rooms and checks if its the one the user is in
                for (DataSnapshot dst : snapshot.getChildren()) {
                    GameRoom room = dst.getValue(GameRoom.class);
                    //if it is the specific game-room
                    if (room.getGameRoomId() == gameRoom.getGameRoomId()) {
                        found = true;
                        gameRoom = room; //updates the game-room that we saved here
                        //updates the users who is the current player
                        mark_curr_player(gameRoom.getPlayerByIndex(gameRoom.getCurrPlayerIndex()).getUser().getUserName());

                        //if the game finished
                        if (gameRoom.getFinishGame() && !finished){
                            finished = true;
                            find_player_index();
                            UserStatistics user = gameRoom.getPlayerByIndex(playerIndex).getUser(); //gets the user's statistics

                            gameRoom.updatePlayerGameOver(playerIndex); //update the user (his score and num of games)

                            hathatul_show(); //shows the hathatul logo

                            end_game(); //shows all the cards to the player.

                            //update in the firebase
                            database.getReference().child("users").child(user.getUserName()).child("stat").setValue(user);
                            q.removeEventListener(this); //stop listening to changes bcs the game over
                            break;
                        }

                        //if the players swapped cards
                        if (gameRoom.getSwappedCardIndexes().get(0) != -1){
                            // peek the swapped card
                            peek_card(gameRoom.getSwappedCardIndexes().get(playerIndex));

                            // init
                            gameRoom.getSwappedCardIndexes().set(0, -1);
                            gameRoom.getSwappedCardIndexes().set(1, -1);
                        }
                    }
                }
                //if the game is not in the firebase
                if (!found){
                    q.removeEventListener(this);
                    Intent in = new Intent(Game.this, AfterGame.class);
                    startActivity(in);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        init_buttons_and_texts();
    }

    @Override
    public void onClick(View v) {
        if (gameRoom.getCurrPlayerIndex() == playerIndex) { //checking that it's this player turn

            // if the user wants to finish the game
            if (v == finishGameBtn) {
                gameRoom.clcPoints();
                gameRoom.finishGame();
                this.finish_turn();
            }

            //if the user chooses to take a card from the deck
            else if (!switchOldCardWithNew && v == deckBtn) {
                get_card_from_deck(false);
            }

            //if the user chooses to take a card from the used-cards
            else if (!switchOldCardWithNew && v == usedCardsBtn) {
                currCard = gameRoom.getPack().takeLastUsedCard();
                switchOldCardWithNew = true;
            }

            //if the user wants to switch a card (one of his four cards)
            else if (!peekCard && switchOldCardWithNew && this.card_btn_pressed(v)) {
                int place = this.get_card_index(v);
                Card swappedCard = gameRoom.getPlayerByIndex(playerIndex).getSpecificCard(place); //saves the card that we throw
                gameRoom.getPack().throwCard(swappedCard); //throws the old card to the used-cards
                gameRoom.getPlayerByIndex(playerIndex).swapCard(place, currCard); //swaps the new card with the old one
                usedCardsBtn.setImageResource(swappedCard.getImg()); //shows the new used-card

                switchOldCardWithNew = false;

                //checking if the user had a pull-two card
                if (pullTwoCard) {
                    pullTwoCard = false;
                    get_card_from_deck(true);
                }

                //if the user didn't had a pull-two card
                else {
                    finish_turn();
                }

            }

            //if the user got a "peek-card"
            else if (peekCard && !switchOldCardWithNew && this.card_btn_pressed(v)) {
                this.peek_card(this.get_card_index(v));
                peekCard = false;

                finish_turn();
            }

            //if the user got a "swap-card"
            else if (swapCard && !switchOldCardWithNew && this.opponents_btn_pressed(v)){
                chosenOpponentCardIndex = this.get_opponent_card_index(v);
                swap_card();

            }
            else if(swapCard && !switchOldCardWithNew && this.card_btn_pressed(v)){
                chosenCardIndex = this.get_card_index(v);
                swap_card();
            }

        }

        //if the user pressed on the button that appeared after the game-over
        if (v == gameOverBtn){
            Intent in = new Intent(Game.this, AfterGame.class);
            startActivity(in);
            //finish();
        }

    }

    /**
     * finds the players index
     */
    private void find_player_index(){
        for (int i = 0; i < numOfPlayers; i++){
            if (gameRoom.getPlayerByIndex(i).getUser().equals(MainActivity.user.stat)) {
                playerIndex = i;
            }
        }
    }

    /**
     * initializes the buttons and put them on listener
     * writes the users username on the screen
     */
    private void init_buttons_and_texts(){
        // deck
        deckBtn = (ImageButton)findViewById(R.id.deckBtn);
        deckBtn.setOnClickListener(this);
        usedCardsBtn = (ImageButton)findViewById(R.id.usedCardsBtn);
        usedCardsBtn.setOnClickListener(this);
        usedCardsBtn.setImageResource(gameRoom.getPack().getLastUsedCard().getImg());
        finishGameBtn = (Button) findViewById(R.id.finishGameBtn);
        finishGameBtn.setOnClickListener(this);
        finishGameIv = findViewById(R.id.finishGameIv);
        gameOverBtn= (Button) findViewById(R.id.gameOverBtn);
        gameOverBtn.setOnClickListener(this);
        gameOverBtn.setVisibility(View.GONE);

        // user's
        userNameTV = (TextView)findViewById(R.id.userNameTV);
        userNameTV.setText(gameRoom.getPlayerByIndex(playerIndex ).getUser().getUserName());
        userCard1Btn = (ImageButton)findViewById(R.id.userCard1Btn);
        userCard1Btn.setOnClickListener(this);
        userCard2Btn = (ImageButton)findViewById(R.id.userCard2Btn);
        userCard2Btn.setOnClickListener(this);
        userCard3Btn = (ImageButton)findViewById(R.id.userCard3Btn);
        userCard3Btn.setOnClickListener(this);
        userCard4Btn = (ImageButton)findViewById(R.id.userCard4Btn);
        userCard4Btn.setOnClickListener(this);

        // upper user's
        player2nameTV = (TextView)findViewById(R.id.player2nameTV);
        player2nameTV.setText(gameRoom.getPlayerByIndex((playerIndex + 1) % numOfPlayers).getUser().getUserName());
        card1Btn = (ImageButton)findViewById(R.id.card1Btn);
        card1Btn.setOnClickListener(this);
        card2Btn = (ImageButton)findViewById(R.id.card2Btn);
        card2Btn.setOnClickListener(this);
        card3Btn = (ImageButton)findViewById(R.id.card3Btn);
        card3Btn.setOnClickListener(this);
        card4Btn = (ImageButton)findViewById(R.id.card4Btn);
        card4Btn.setOnClickListener(this);

    }

    /**
     * checking if the user pressed on one of the cards
     * @param v: the button that the user pressed on
     * @return: if the button was one of the cards
     */
    private boolean card_btn_pressed(View v) {
        return v == userCard1Btn || v == userCard2Btn || v == userCard3Btn || v == userCard4Btn;
    }

    /**
     * checking if the user pressed on one of the opponent's cards
     * @param v: the button that the user pressed on
     * @return: if the button was one of opponent's the cards
     */
    private boolean opponents_btn_pressed(View v){
        return v == card1Btn || v == card2Btn || v == card3Btn || v == card4Btn;
    }

    /**
     * clc the index of the chosen card
     * @param v: the button that the user chose
     * @return: the index of the card the user chose (0-3)
     */
    private int get_card_index(View v){
        int place = -1;
        if (v == userCard1Btn) {place = 0;}
        else if (v == userCard2Btn) {place = 1;}
        else if (v == userCard3Btn) {place = 2;}
        else if (v == userCard4Btn) {place = 3;}
        return place;
    }

    /**
     * clc the index of the chosen card
     * @param v: the button that the user chose
     * @return: the index of the card the user chose (0-3)
     */
    private int get_opponent_card_index(View v){
        int place = -1;
        if (v == card1Btn) {place = 0;}
        else if (v == card2Btn) {place = 1;}
        else if (v == card3Btn) {place = 2;}
        else if (v == card4Btn) {place = 3;}
        return place;
    }

    /**
     * when the user choose to take a new card from the deck.
     * gets a random card, shows it to the user
     * and lets the user choose if he want to use it or throw
     * @param onlyRegulars: true- if we want only regulars cards,
     *                    false - if we want any kind of card (regular and special)
     */
    private void get_card_from_deck(boolean onlyRegulars){
        // gets a random card from the pack
        currCard = gameRoom.getPack().getRandomCard(onlyRegulars);

        // building the dialog
        AlertDialog.Builder ImageDialog = new AlertDialog.Builder(Game.this);
        ImageView showImage = new ImageView(Game.this);
        showImage.setImageResource(currCard.getImg());
        ImageDialog.setView(showImage);
        ImageDialog.setCancelable(false);

        // if the user doesn't want to use the card
        ImageDialog.setNegativeButton("throw", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                usedCardsBtn.setImageResource(currCard.getImg());
                gameRoom.getPack().throwCard(currCard);

                /** checking if the new card is a special card **/

                // a peek card
                if (currCard.getValue() == 10) {
                    Toast.makeText(Game.this, "choose a card to see :)", Toast.LENGTH_LONG).show();
                    peekCard = true;
                }

                // a pull-two card
                else if (currCard.getValue() == 11) {
                    pullTwoCard = true;
                    get_card_from_deck(true);
                }

                else if (pullTwoCard) {
                    pullTwoCard = false;
                    get_card_from_deck(true);
                }

                // a swap card
                else if (currCard.getValue() == 12){
                    Toast.makeText(Game.this, "choose cards to swap :)", Toast.LENGTH_LONG).show();
                    swapCard = true;
                }

                else {
                    finish_turn();
                }
            }
        });

        // if the user want to use the card
        ImageDialog.setPositiveButton("use", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                switchOldCardWithNew = true;
            }
        });
        ImageDialog.show();
    }

    /**
     * handle the peek-card.
     * when the user throws a new peek card, it lets them to choose a card to see.
     * in this function, it shows the card that the user chose.
     * @param index: the index of the card to see
     */
    private void peek_card(int index){
        Card cardToSee = gameRoom.getPlayerByIndex(playerIndex).getSpecificCard(index);

        // building the dialog to show the card
        AlertDialog.Builder ImageDialog = new AlertDialog.Builder(Game.this);
        ImageDialog.setTitle("PEEK");
        ImageView showImage = new ImageView(Game.this);
        showImage.setImageResource(cardToSee.getImg());
        ImageDialog.setView(showImage);
        ImageDialog.setCancelable(false);

        ImageDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1) { }
        });
        ImageDialog.show();
        peekCard = false;
    }

    /**
     * handle the swap-card
     * works only after the user chose 2 cards to swap
     * the function swaps between the cards and finishes the turn
     */
    private void swap_card(){
        if (swapCard && this.chosenCardIndex != -1 && this.chosenOpponentCardIndex != -1){
            // saving the cards
            Card usersCard = gameRoom.getPlayerByIndex(playerIndex).getCards().get(chosenCardIndex);
            Card opponentCard = gameRoom.getPlayerByIndex((playerIndex + 1) % 2).getCards().get(chosenOpponentCardIndex);

            // swaps
            gameRoom.getPlayerByIndex(playerIndex).swapCard(chosenCardIndex, opponentCard);
            gameRoom.getPlayerByIndex((playerIndex + 1) % 2).swapCard(chosenOpponentCardIndex, usersCard);
            if(playerIndex == 0){
                gameRoom.swappedCards(this.chosenCardIndex, this.chosenOpponentCardIndex);
            }
            else {
                gameRoom.swappedCards(this.chosenOpponentCardIndex, this.chosenCardIndex);
            }

            // init
            this.chosenCardIndex = -1;
            this.chosenOpponentCardIndex = -1;
            swapCard = false;

            finish_turn();
        }
        else{
            Toast.makeText(Game.this, "choose one more card to swap :)", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * when the user finished his turn.
     * sends the changes to the firebase and puts the CurrPlayerIndex to the next player
     */
    private void finish_turn(){
        gameRoom.setCurrPlayerIndex((playerIndex + 1) % numOfPlayers);
        myRef.child(gameRoom.getGameRoomId() + "").setValue(gameRoom);
    }

    /**
     * shows all the cards to the player.
     * use when the game over
     */
    private void end_game(){
        //showing the player's cards
        ArrayList <Card> playerCard = gameRoom.getPlayerByIndex(playerIndex).getCards();
        userCard1Btn.setImageResource(playerCard.get(0).getImg());
        userCard2Btn.setImageResource(playerCard.get(1).getImg());
        userCard3Btn.setImageResource(playerCard.get(2).getImg());
        userCard4Btn.setImageResource(playerCard.get(3).getImg());

        //showing the other player's cards
        playerCard = gameRoom.getPlayerByIndex((playerIndex + 1) % numOfPlayers).getCards();
        card1Btn.setImageResource(playerCard.get(0).getImg());
        card2Btn.setImageResource(playerCard.get(1).getImg());
        card3Btn.setImageResource(playerCard.get(2).getImg());
        card4Btn.setImageResource(playerCard.get(3).getImg());

        gameOverBtn.setVisibility(View.VISIBLE);
        finishGameBtn.setVisibility(View.GONE);
        deckBtn.setEnabled(false);
        usedCardsBtn.setEnabled(false);

        player_score = gameRoom.getPlayerByIndex(playerIndex).getGameScore();
        opponent_score = gameRoom.getPlayerByIndex((playerIndex + 1) % 2).getGameScore();

    }

    /**
     * showing the HATHATUL logo instead of the decks.
     * use when one of the user wants to finish the game
     */
    private void hathatul_show(){
        new CountDownTimer(2000, 1000) { //show for 2 sec
            //in those 2 sec - show the picture
            public void onTick(long millisUntilFinished) {
                finishGameIv.setVisibility(View.VISIBLE);
                finishGameIv.setImageResource(R.drawable.finishgame);
                deckBtn.setVisibility(View.GONE);
                usedCardsBtn.setVisibility(View.GONE);
            }

            //after the 2 sec - return the decks
            public void onFinish() {
                finishGameIv.setVisibility(View.GONE);
                deckBtn.setVisibility(View.VISIBLE);
                usedCardsBtn.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    /**
     * marking the name of the player who need to play now
     * @param userName: the username of the player who need to play now
     */
    private void mark_curr_player(String userName){
        //if it's the player's turn
        if (userName.equals(gameRoom.getPlayerByIndex(playerIndex).getUser().getUserName())){
            userNameTV.setBackgroundColor(Color.argb(150, 0, 128, 255));
            player2nameTV.setBackgroundColor(Color.argb(0, 255, 255, 255));
        }
        //if it's the other player's turn
        else{
            player2nameTV.setBackgroundColor(Color.argb(150, 0, 128, 255));
            userNameTV.setBackgroundColor(Color.argb(0, 255, 255, 255));
        }
        usedCardsBtn.setImageResource(gameRoom.getPack().getLastUsedCard().getImg());
    }
}