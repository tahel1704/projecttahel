package com.example.projecttahel;

import androidx.annotation.NonNull;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends BasicActivity implements View.OnClickListener {
    //xml things
    Button btnSignUp, btnLogin, btnLoginSignup;
    ImageButton btnBack;
    EditText etName, etPass;
    TextView tvName, tvPass, tvTitle;

    public static Account user; //the user. public static bcs we uses it in other intent

    NoInternetReceiver networkRec = new NoInternetReceiver(); //checks if there is no internet

    Intent musicIntent; //music intent (bcs we want it to work always)

    //db things
    FirebaseDatabase database;
    DatabaseReference myRef;
    ValueEventListener listener;
    Query q;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //the buttons of signup / login
        setContentView(R.layout.activity_main);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        //the buttons and texts after the user choose to signup / login
        btnLoginSignup = findViewById(R.id.btnLoginNow);
        btnLoginSignup.setOnClickListener(this);
        etName = findViewById(R.id.etName);
        etPass = findViewById(R.id.etPass);
        tvName = findViewById(R.id.tvName);
        tvPass = findViewById(R.id.tvPass);
        tvName = findViewById(R.id.tvName);
        tvPass = findViewById(R.id.tvPass);
        tvTitle = findViewById(R.id.tvTitle);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        //getting the firebase instance
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        changeAppToMain();

        //plays the music
        musicIntent = new Intent(MainActivity.this, BackgroundMusicService.class);
        startService(musicIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkRec, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(networkRec);
    }

    @Override
    protected void onDestroy() {
        stopService(musicIntent);
        super.onDestroy();
    }

    @SuppressLint({"SetTextI18n", "NonConstantResourceId"})
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            // if the user chose to login
            case R.id.btnLogin:
                changeAppToLoginSignup();
                tvTitle.setText("LOGIN");
                btnLoginSignup.setText("login now");
                break;

            // if the user chose to signup
            case R.id.btnSignUp:
                changeAppToLoginSignup();
                tvTitle.setText("SIGN-UP");
                btnLoginSignup.setText("sign up now");
                break;

            // if the user want to try to login or signup (w\ his username and password)
            case R.id.btnLoginNow:
                // if the button is on SIGN-UP mode
                if (tvTitle.getText().equals("SIGN-UP")){
                    signup();
                }

                // if the button is on LOGIN mode
                else if (tvTitle.getText().equals("LOGIN")){
                    login();
                }

                // if the button is on unknown mode (shouldn't happen)
                else{
                    Toast.makeText(MainActivity.this, "something wrong happened", Toast.LENGTH_LONG).show();
                    break;
                }


                //adding listener to the user - works all the time bcs we want it to update when the user get scores and games
                q = database.getReference().child("users").orderByKey();
                listener = q.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (user != null) { //if the user is not null
                            for (DataSnapshot dst : snapshot.getChildren()) { //move on all the game-rooms and checks if its the one the user is in
                                if (dst != null)
                                {
                                    Account account = dst.getValue(Account.class);
                                    if (account.equals(user)) {
                                        user = account;
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });



                // if the user want to go back (to choose if login or signup)
            case R.id.btnBack:
                changeAppToMain();
                break;

            // unknown button (shouldn't happen)
            default:
                throw new RuntimeException("Unknown button ID");
        };
    }

    /**
     * changes the app to the login / sign up "screen".
     * - hides the "login" and "signup" buttons
     * - make the login / signup things visible (the title, name, password, back button...)
     */
    private void changeAppToLoginSignup(){
        btnLogin.setVisibility(View.GONE);
        btnSignUp.setVisibility(View.GONE);

        btnLoginSignup.setVisibility(View.VISIBLE);
        tvName.setVisibility(View.VISIBLE);
        tvPass.setVisibility(View.VISIBLE);
        etName.setVisibility(View.VISIBLE);
        etPass.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
    }

    /**
     * changes the app to the menu "screen". probably used when the user press the "back" button
     * - make the "login" and "signup" buttons visible
     * - hides the login / signup things (the title, name, password, back button...)
     */
    private void changeAppToMain(){
        btnLogin.setVisibility(View.VISIBLE);
        btnSignUp.setVisibility(View.VISIBLE);

        btnLoginSignup.setVisibility(View.GONE);
        tvName.setVisibility(View.GONE);
        tvPass.setVisibility(View.GONE);
        etName.setVisibility(View.GONE);
        etPass.setVisibility(View.GONE);
        tvTitle.setVisibility(View.GONE);
        btnBack.setVisibility(View.GONE);
    }

    /**
     * checks if the user already signed up and not active in other place.
     * used when the user pressed "login now" and enter his username and password
     */
    private void login(){
        //saving the username and password that the user entered
        String newUserName = etName.getText().toString().trim().toString();
        String newPassword = etPass.getText().toString().trim().toString();
        Account newAccount = new Account(newUserName, newPassword);

        // if the user didn't entered username and password
        if((newUserName.length() == 0) || newPassword.length() == 0) {
            Toast.makeText(this, "you gotta enter username and password", Toast.LENGTH_LONG).show();
        }

        // if the user entered username and password
        else {
            Query q = database.getReference().child("users").orderByValue(); // gets the users table from the firebase

            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean userAlreadyExist = false;

                    //move on all the usernames and checks if its the same as the new one
                    for (DataSnapshot dst : snapshot.getChildren()) {
                        Account tempUser = dst.getValue(Account.class);
                        if (tempUser.getUser_name().equals(newUserName) && tempUser.getPassword().equals(newPassword)) {
                            Toast.makeText(MainActivity.this, "LOGGED IN!", Toast.LENGTH_LONG).show();
                            userAlreadyExist = true;
                            user = tempUser;
                        }
                    }
                    //if the user doesnt exist yet
                    if (!userAlreadyExist) {
                        Toast.makeText(MainActivity.this, "can't connect", Toast.LENGTH_LONG).show();
                    }

                    else
                    {
                        Intent i = new Intent(MainActivity.this, JoinCreateRoom.class);
                        startActivity(i);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) { }
            });
        }
    }

    /**
     * tries to sign up the user
     * checks if the user didn't signed up already and if the username and the password are valid
     * used when the user pressed "sign-up now" and enter his username and password
     */
    private void signup(){
        //saving the new username and password
        String newUserName = etName.getText().toString().trim().toString();
        String newPassword = etPass.getText().toString().trim().toString();

        // if the user didn't entered username and password
        if((newUserName.length() == 0) || newPassword.length() == 0) {
            Toast.makeText(this, "you gotta enter username and password", Toast.LENGTH_LONG).show();
        }

        // if the user entered username and password
        else {
            Query q = database.getReference().child("users").orderByValue();
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    boolean userAlreadyExist = false;

                    //move on all the usernames and checks if its the same as the new one
                    for (DataSnapshot dst : snapshot.getChildren()) {
                        String tempUserName = dst.getKey();
                        if (tempUserName.trim().equals(newUserName)) {
                            Toast.makeText(MainActivity.this, "the username already exist", Toast.LENGTH_LONG).show();
                            userAlreadyExist = true;
                        }
                    }
                    //if the user doesnt exist yet
                    if (!userAlreadyExist) {
                        Toast.makeText(MainActivity.this, "sign-up!!", Toast.LENGTH_LONG).show();
                        user = new Account(newUserName, newPassword);
                        myRef = database.getReference().child("users");
                        myRef.child(newUserName).setValue(user);
                        Intent i = new Intent(MainActivity.this, JoinCreateRoom.class);
                        startActivity(i);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }
}
