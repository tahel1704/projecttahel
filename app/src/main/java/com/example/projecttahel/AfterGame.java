package com.example.projecttahel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

public class AfterGame extends BasicActivity implements View.OnClickListener {

    FirebaseDatabase database; //db
    DatabaseReference myRef; //db reference
    GameRoom gameRoom; //the game that the user played
    int playerIndex; //the user's index in the game
    ArrayList<Player> userList =new ArrayList<>(); //list of the players and their score

    //xml things
    ListView winnersLv;
    ImageButton backBtn;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_game);

        //inits the db things
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("game-rooms");

        //connects buttons and list-view
        winnersLv = findViewById(R.id.winnersLv);
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);

        //gets info from the game
        gameRoom = WaitingRoom.gameRoom;
        playerIndex = Game.playerIndex;
        int player_score = Game.player_score;
        int opponent_score = Game.opponent_score;

        //puts the scores in the array-list
        ArrayList<String> scores = new ArrayList<String>();
        scores.add("player " + gameRoom.getPlayerByIndex(playerIndex).getUser().getUserName() + "\t-\t" + player_score + " points");
        scores.add("player " + gameRoom.getPlayerByIndex((playerIndex + 1) % 2).getUser().getUserName() + "\t-\t" + opponent_score + " points");

        //shows the scores to the player
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,scores);
        winnersLv.setAdapter(adapter);

        //deletes the game from the firebase
        if (gameRoom.isPlayerAdmin(playerIndex)) {

            myRef.child(gameRoom.getGameRoomId() + "").setValue(null);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == backBtn){
            //moves to the join-create room screen
            Intent in = new Intent(this, JoinCreateRoom.class);
            in.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivityIfNeeded(in, 0);
        }
    }
}