package com.example.projecttahel;

import android.graphics.Bitmap;
import androidx.annotation.Nullable;

/**
 * this class is meant to represent a card in a game by his value and picture.
 * there are 13 kinds of cards. 10 regular and 3 special
 */

public class Card {
    private int value;
    private int img;

    public Card(){}

    public Card(int value) {
        this.value = value;
    }

    public Card(int value, int img) {
        this.value = value;
        this.img = img;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return value == card.value &&
                img == card.img;
    }


    /***********          GETTERS & SETTERS          **********/

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
