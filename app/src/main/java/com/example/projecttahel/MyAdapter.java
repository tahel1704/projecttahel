package com.example.projecttahel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Player> {

    ArrayList<Player> statusList = new ArrayList<>();

    public MyAdapter(Context context, int textViewResourceId, ArrayList<Player> objects) {
        super(context, textViewResourceId, objects);
        statusList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.top10, null);
        TextView nameTV = (TextView) v.findViewById(R.id.nameTV);
        TextView scoreTV = (TextView) v.findViewById(R.id.scoreTV);
        nameTV.setText(statusList.get(position).getUser().getUserName());
        scoreTV.setText(statusList.get(position).getUser().getScore() + "");
        return v;

    }

}