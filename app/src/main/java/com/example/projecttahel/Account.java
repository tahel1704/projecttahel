package com.example.projecttahel;
import androidx.annotation.Nullable;

import java.io.Serializable;
/**
 * this class is meant to save a users's login information:
 *      username
 *      password
 *      statistics information
 **/

@SuppressWarnings("serial") //With this annotation we are going to hide compiler warnings
public class Account implements Serializable{
    private String user_name;
    private String password;
    public UserStatistics stat;

    public Account(String user_name, String password) {
        this.user_name = user_name;
        this.password = password;
        this.stat = new UserStatistics(user_name);
    }

    public Account(){}

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        return obj.getClass() == Account.class &&
                ((Account)obj).user_name == this.user_name &&
                ((Account)obj).password.equals(this.password);
    }


    /***********          GETTERS & SETTERS          **********/

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getPassword() {
        return password;
    }

}
