package com.example.projecttahel;

import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 * the class represent a player in the game. each player have:
 *      an array-list of 4 cards,
 *      the statistics of the user,
 *      the points in the current game (after finishing)
 * the class know how to manage the players moves in the game -
 * clc the points at the end of the game, getting a card by his index, swap cards...
 */
public class Player {
    private ArrayList<Card> cards;
    private UserStatistics user;
    private int gameScore;

    public Player(){}

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        return obj.getClass() == Player.class &&
                ((Player)obj).gameScore == this.gameScore &&
                ((Player)obj).user.equals(this.user) &&
                ((Player)obj).cards.equals(this.cards);
    }

    public Player(UserStatistics user, CardPackage deck) {
        this.user = user;
        this.gameScore = 0;
        this.cards = new ArrayList<Card>();
        for (int i = 0; i < 4; i++)
        {
            this.cards.add(deck.getRandomCard(false));
        }
    }


    /***
     * gets the Card in a specific index (0-3)
     * @param index: the index of the card (0-3), left to right
     * @return the Card in the index. if the index is not valid, null.
     */
    public Card getSpecificCard(int index){
        if (index >= 0 && index <= 3)
        {
            return this.cards.get(index);
        }
        return null;
    }

    /***
     * calculate the sum of the values of the players cards
     * @param deck: the cards in the game - in case the user have a special card
     * @return the sum of the values of the players cards
     */
    public int clcSum(CardPackage deck){
        int sum = 0;
        for (int i = 0; i < this.cards.size(); i++) {
            if (this.cards.get(i).getValue() < 10) { //not a special card
                sum += this.cards.get(i).getValue();
            }
            else {
                Card randNewCard = deck.getRandomCard(true);
                sum += randNewCard.getValue();
                this.cards.set(i, randNewCard);
            }
        }
        return sum;
    }

    /***
     * swapping a card in the users cards by index
     * @param index: the index of the card to replace (0-3)
     * @param newCard: the new card to put in that index
     * @return the old card (in the index)
     */
    public Card swapCard(int index, Card newCard){
        Card oldCard = this.cards.get(index);
        this.cards.set(index, newCard);
        return oldCard;
    }


    /***********          GETTERS & SETTERS          **********/

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public void setUser(UserStatistics user) {
        this.user = user;
    }

    public int getGameScore() {
        return gameScore;
    }

    public void setGameScore(int gameScore) {
        this.gameScore = gameScore;
    }

    public UserStatistics getUser() {
        return user;
    }
}
